#include <fstream>
#include <iostream>
#include "eratosphen.hpp"

void Eratosthenes(int massiv[], int n)
{
	int i, j;
	
	i=2;
	
	while (i <= n) {
		if (massiv[i] != 0) {
			j = i + i;
			while (j <= n) {
				massiv[j] = 0;
				j = j + i;
			}
		}
		i += 1;
	}
	
	std::cout<<"Список простых чисел:\n";
	
	int count = 0;
	
	for (i=2; i<=n; i++)
	{
		if (massiv[i]!=0) {std::cout<<" "<<i; count++;}
	}
	std::cout<<"\n" << count;
}

int WriteNumbersToFile(int massiv[], int n)
{
	std::ofstream outf{ "numbers.dat" };

    int j;

	for (j=2; j<=n; j++)
	{
		if (massiv[j]!=0) outf<<j<<"\n";
	}
	return 0;
}


int main()
{
	setlocale(LC_ALL,"Rus");
	int i, n;
	std::cout<<"Введите число: ";
	std::cin>>n;
	std::cout<<std::endl;
	int *massiv=new int[n];
	for (i=2; i<=n; i++) massiv[i]=i;
	Eratosthenes(massiv, n);
	std::cout<<std::endl;	
	return WriteNumbersToFile(massiv, n);
}

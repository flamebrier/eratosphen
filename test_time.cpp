#include <chrono>
#include <fstream>
#include <iostream>

void Eratosthenes(int massiv[], int n)
{
	int i, j;
	
	i=2;
	
	while (i <= n) {
		if (massiv[i] != 0) {
			j = i + i;
			while (j <= n) {
				massiv[j] = 0;
				j = j + i;
			}
		}
		i += 1;
	}
}

int main()
{
	using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::duration;
    using std::chrono::milliseconds;
    
	setlocale(LC_ALL,"Rus");
	int i, n;
	std::cout<<"Введите число: ";
	std::cin>>n;
	std::cout<<std::endl;
	
	auto t1 = high_resolution_clock::now();
	int *massiv=new int[n];
	
	for (i=2; i<=n; i++) massiv[i]=i;
	Eratosthenes(massiv, n);
	
	auto t2 = high_resolution_clock::now();
	auto ms_int = duration_cast<milliseconds>(t2 - t1);
	
	std::cout<<std::endl;
	std::cout<<ms_int.count()<< "ms\n";
	
	return 0;
}

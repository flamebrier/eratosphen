#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

int WriteNumbersToFile(int massiv[], int n)
{
	std::ofstream outf{ "numbers.dat" };

    int j;

	for (j=2; j<=n; j++)
	{
		if (massiv[j]!=0) outf<<j<<"\n";
	}
	return 0;
}


int main()
{
	setlocale(LC_ALL,"Rus");
	int i, n;
	std::cout<<"Введите число: ";
	std::cin>>n;
	std::cout<<std::endl;
	int *massiv=new int[n];
	for (i=2; i<=n; i++) massiv[i]=i;
	WriteNumbersToFile(massiv, n);
	
	std::ifstream infile("numbers.dat");
	
	std::string line;
	
	int *fromFile=new int[n];
	i = 2;
	
while (std::getline(infile, line))
{
    std::istringstream iss(line);
    int a;
    if (!(iss >> a)) { break; } // error
    
    fromFile[i++] = a;
}

int ko = 0;


for (i = 2; i <= n; i++) {
	if (fromFile[i] != massiv[i]) {
		ko++;
}
}


if (ko != 0) {
std::cout << "Число ошибок в записи файла:" << ko << "\n";
} else {
	std::cout << "OK";
	}
	return 0;
}

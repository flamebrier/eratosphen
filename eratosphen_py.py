# -*- coding: utf-8 -*-
def Eratosthenes(massiv, n):
	i = 2
	while i <= n:
		if massiv[i] != 0:
			j = i + i
			while j <= n:
				massiv[j] = 0
				j = j + i
		i += 1
	massiv = set(massiv)
	print("Список простых чисел: ")
	s = ""
	for a in massiv:
		if a != 0:
			s += str(a) + " "
	print s
	
def WriteNumbersToFile(massiv):
	f = open("numbers_py.dat", "wb+")
	
	s = ""
	for a in massiv:
		if a != 0:
			s += str(a) + "\n"
	f.write(s)
	
	f.close()


print("Введите число: ")
n = int(input())
massiv = []
for i in range(n + 1):
	massiv.append(i)
    
massiv[1] = 0
Eratosthenes(massiv, n)
WriteNumbersToFile(massiv)

#include <fstream>
#include <iostream>
#include <sstream>

void Eratosthenes(int massiv[], int n)
{
	int i, j;
	
	i=2;
	
	while (i <= n) {
		if (massiv[i] != 0) {
			j = i + i;
			while (j <= n) {
				massiv[j] = 0;
				j = j + i;
			}
		}
		i += 1;
	}
}

int TestPrimes(int massiv[], int n) {
	int i, res;
	
	res = 0;
	i=2;
	
	while (i <= n) {
		if (massiv[i] != 0) {
			for (int j = 2; i < massiv[i] / 2; j++) {
				if (massiv[i] % j == 0) {
					res++;
				}
			}
		}
		i += 1;
	}
	
	return res;
	}


int main(int argc, char *argv[])
{
	int n, i;
	setlocale(LC_ALL,"Rus");
	std::cout<<"Введите число: ";
	std::cin>>n;
	std::cout<<std::endl;
	
	
	int *massiv=new int[n];
	for (i=2; i<=n; i++) massiv[i]=i;
	
	Eratosthenes(massiv, n);
	std::cout << std::endl;
	std::cout << TestPrimes(massiv, n);
}
